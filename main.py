import os
import pygame
from parameters import *
from UI import *
from constructor import *
from random_generator import *


def main():
    # Настройка окна приложения и её некоторых деталей
    pygame.init()
    pygame.mixer.init()
    if TEST_MODE:
        game_window = pygame.display.set_mode((WIDTH, HEIGHT))
    else:
        game_window = pygame.display.set_mode((WIDTH, HEIGHT), pygame.FULLSCREEN | pygame.SCALED)
    pygame.display.set_caption("Randomizer")
    pygame.display.set_icon(pygame.image.load(file_img_logo))
    clock = pygame.time.Clock()


    # Основной цикл программы
    program_running = True
    while program_running:
        # Установка FPS
        clock.tick(FPS)


        # Ввод процесса (события)
        ## Указание нажатых клавиш
        keystate = pygame.key.get_pressed()
        ## Указание нажатых кнопок мыши и её позиции
        mouse_key = pygame.mouse.get_pressed()
        mouse_pos = pygame.mouse.get_pos()
        ## Проверка конкретных событий
        for event in pygame.event.get():
            for obj in sprites_event:
                obj.event_check(event, keystate, mouse_key, mouse_pos)
            if event.type == pygame.QUIT:
                program_running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    program_running = False


        # Обновление
        all_sprites.update()
        if butt_exit.fl_exit == True:
            program_running = False


        # Рендеринг
        game_window.fill(BLACK)
        screen.fill((70, 70, 70, 255))
        # all_sprites.draw(screen)
        # for obj in sprites_layer:
        #     obj.cleaning()
        for obj in all_sprites:
            obj.render()
        game_window.blit(screen, (0, 0))
        pygame.display.flip()

    pygame.quit()



if __name__ == '__main__':
    main()