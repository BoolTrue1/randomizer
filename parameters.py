import os
import sys
import pygame


def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath('.')
    
    return os.path.join(base_path, relative_path)


# Инициализация pygame
pygame.init()


# # Указание путей к папкам
# folder_game = os.path.dirname(__file__)
# folder_img = os.path.join(folder_game, 'img')
# folder_snd = os.path.join(folder_game, 'snd')
# folder_font = os.path.join(folder_game, 'font')

# folder_butt_pl_min = os.path.join(folder_img, 'Buttons_plus_minus')
# folder_exit = os.path.join(folder_img, 'Exit')


# # Указание путей к файлам
# file_img_logo = os.path.join(folder_img, 'logo.png')
# file_img_layer_exit_pass = os.path.join(folder_exit, 'exit_pass.png')
# file_img_layer_exit_act = os.path.join(folder_exit, 'exit_act.png')

# file_img_layer_plus_pass = os.path.join(folder_butt_pl_min, 'plus_pass.png')
# file_img_layer_plus_act = os.path.join(folder_butt_pl_min, 'plus_act.png')
# file_img_layer_minus_pass = os.path.join(folder_butt_pl_min, 'minus_pass.png')
# file_img_layer_minus_act = os.path.join(folder_butt_pl_min, 'minus_act.png')


# file_font_press_start = os.path.join(folder_font, 'PressStart2P-Regular.ttf')


# Указание путей к файлам
file_img_logo = resource_path('img\\logo.png')
file_img_layer_exit_pass = resource_path('img\\Exit\\exit_pass.png')
file_img_layer_exit_act = resource_path('img\\Exit\\exit_act.png')

file_img_layer_plus_pass = resource_path('img\\Buttons_plus_minus\\plus_pass.png')
file_img_layer_plus_act = resource_path('img\\Buttons_plus_minus\\plus_act.png')
file_img_layer_minus_pass = resource_path('img\\Buttons_plus_minus\\minus_pass.png')
file_img_layer_minus_act = resource_path('img\\Buttons_plus_minus\\minus_act.png')

file_font_press_start = resource_path('font\\PressStart2P-Regular.ttf')


# Тестовый режим
TEST_MODE = False


# Указание разрешения окна и fps
info_object = pygame.display.Info()
if TEST_MODE:
    WIDTH = 1600 // 2
    HEIGHT = 900 // 2
else:
    WIDTH = info_object.current_w
    HEIGHT = info_object.current_h
FPS = 60


# Указание основных цветов
BLACK = (0, 0, 0, 255)
GREY = (128, 128, 128, 255)
WHITE = (255, 255, 255, 255)
RED = (255, 0, 0, 255)
ORANGE = (255, 128, 0, 255)
YELLOW = (255, 255, 0, 255)
GREEN = (0, 255, 0, 255)
BLUE = (0, 0, 255, 255)

