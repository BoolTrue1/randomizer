# import os
# import pygame
# from parameters import *
from UI import *


# Создание основной поверхности программы (холста)
screen = pygame.Surface((WIDTH, HEIGHT))


# Создание специальной группы для спрайтов
all_sprites = pygame.sprite.Group()
sprites_event = pygame.sprite.Group()
sprites_layer = pygame.sprite.Group()



# Создание объектов (спрайтов)

layers_canvas = Layers([all_sprites, sprites_layer], screen, [0.8, 0.4], 'c', [0.5, 0.45], GREY, '')


butt_exit = Button([all_sprites, sprites_event], screen, [0.1125, 0.1], 'tr', [1.0, 0], GREY)
butt_exit.add_indicated_events({
    'pointing':  [butt_exit.change_img_active, butt_exit.change_color_active],
    'not_pointing': [butt_exit.change_img_passive, butt_exit.change_color_passive],
    'press': [butt_exit.game_quit],
})
butt_exit.colors['color_active'] = WHITE  # (255, 255, 255, 0)
butt_exit.set_imgs(file_img_layer_exit_pass, file_img_layer_exit_act)



input_field_1 = Input_field([all_sprites, sprites_event], screen, [0.2, 0.1], 'tl', [0.22, 0.28], \
    (BLACK, (70, 70, 70, 255), (200, 200, 200, 255), (150, 150, 150, 255)), 0.7, file_font_press_start)
input_field_1.add_indicated_events({
    'press_here': [input_field_1.activating_input_field],
    'press_not_here': [input_field_1.passivating_input_field],
    'pointing': [input_field_1.change_color_active],
    'not_pointing': [input_field_1.change_color_passive],
})
input_field_1.nums = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
input_field_1.text = '0'
input_field_1.forced_update()


input_field_2 = Input_field([all_sprites, sprites_event], screen, [0.2, 0.1], 'tl', [-0.32-0.025, 0.28], \
    (BLACK, (70, 70, 70, 255), (200, 200, 200, 255), (150, 150, 150, 255)), 0.7, file_font_press_start)
input_field_2.add_indicated_events({
    'press_here': [input_field_2.activating_input_field],
    'press_not_here': [input_field_2.passivating_input_field],
    'pointing': [input_field_2.change_color_active],
    'not_pointing': [input_field_2.change_color_passive],
})
input_field_2.nums = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
input_field_2.text = '0'
input_field_2.forced_update()



output_field_from = Output_field([all_sprites], screen, [0.1, 0.1], 'tr', [0.22, 0.28], (GREY, GREY, (70, 70, 70, 255)), 'ОТ', 0.6, file_font_press_start)
output_field_to = Output_field([all_sprites], screen, [0.1, 0.1], 'tr', [-0.32-0.025, 0.28], (GREY, GREY, (70, 70, 70, 255)), 'ДО', 0.6, file_font_press_start)


butt_generate = Button([all_sprites, sprites_event], screen, [0.8, 0.1], 't', [0.5, -0.275], GREY) 
butt_generate.add_indicated_events({
    'pointing': [butt_generate.change_color_active, butt_generate.change_color_text_active],
    'not_pointing': [butt_generate.change_color_passive, butt_generate.change_color_text_passive],
    'press': [butt_generate.generating],
    'press_enter': [butt_generate.generating],
})
butt_generate.colors['color_active'] = (200, 200, 200, 255)
butt_generate.input_field_1 = input_field_1
butt_generate.input_field_2 = input_field_2
butt_generate.set_text('Enter', 0.7, [BLACK, (110, 110, 110, 255)], file_font_press_start)


butt_input_field_plus_to_one_1 = Button([all_sprites, sprites_event], screen, [0.025, 0.05], 'tl', [0.42, 0.28], (70, 70, 70, 255))
butt_input_field_plus_to_one_1.field_text = input_field_1
butt_input_field_plus_to_one_1.add_indicated_events({
    'pointing':  [butt_input_field_plus_to_one_1.change_img_active, butt_input_field_plus_to_one_1.change_color_active],
    'not_pointing': [butt_input_field_plus_to_one_1.change_img_passive, butt_input_field_plus_to_one_1.change_color_passive],
    'press': [butt_input_field_plus_to_one_1.change_value_field_plus_one],
})
butt_input_field_plus_to_one_1.colors['color_active'] = (200, 200, 200, 255)
butt_input_field_plus_to_one_1.set_imgs(file_img_layer_plus_pass, file_img_layer_plus_act)


butt_input_field_minus_to_one_1 = Button([all_sprites, sprites_event], screen, [0.025, 0.05], 'tl', [0.42, 0.33], (70, 70, 70, 255))
butt_input_field_minus_to_one_1.field_text = input_field_1
butt_input_field_minus_to_one_1.add_indicated_events({
    'pointing':  [butt_input_field_minus_to_one_1.change_img_active, butt_input_field_minus_to_one_1.change_color_active],
    'not_pointing': [butt_input_field_minus_to_one_1.change_img_passive, butt_input_field_minus_to_one_1.change_color_passive],
    'press': [butt_input_field_minus_to_one_1.change_value_field_minus_one],
})
butt_input_field_minus_to_one_1.colors['color_active'] = (200, 200, 200, 255)
butt_input_field_minus_to_one_1.set_imgs(file_img_layer_minus_pass, file_img_layer_minus_act)



butt_input_field_plus_to_one_2 = Button([all_sprites, sprites_event], screen, [0.025, 0.05], 'tl', [0.855, 0.28], (70, 70, 70, 255))
butt_input_field_plus_to_one_2.field_text = input_field_2
butt_input_field_plus_to_one_2.add_indicated_events({
    'pointing':  [butt_input_field_plus_to_one_2.change_img_active, butt_input_field_plus_to_one_2.change_color_active],
    'not_pointing': [butt_input_field_plus_to_one_2.change_img_passive, butt_input_field_plus_to_one_2.change_color_passive],
    'press': [butt_input_field_plus_to_one_2.change_value_field_plus_one],
})
butt_input_field_plus_to_one_2.colors['color_active'] = (200, 200, 200, 255)
butt_input_field_plus_to_one_2.set_imgs(file_img_layer_plus_pass, file_img_layer_plus_act)


butt_input_field_minus_to_one_2 = Button([all_sprites, sprites_event], screen, [0.025, 0.05], 'tl', [0.855, 0.33], (70, 70, 70, 255))
butt_input_field_minus_to_one_2.field_text = input_field_2
butt_input_field_minus_to_one_2.add_indicated_events({
    'pointing':  [butt_input_field_minus_to_one_2.change_img_active, butt_input_field_minus_to_one_2.change_color_active],
    'not_pointing': [butt_input_field_minus_to_one_2.change_img_passive, butt_input_field_minus_to_one_2.change_color_passive],
    'press': [butt_input_field_minus_to_one_2.change_value_field_minus_one],
})
butt_input_field_minus_to_one_2.colors['color_active'] = (200, 200, 200, 255)
butt_input_field_minus_to_one_2.set_imgs(file_img_layer_minus_pass, file_img_layer_minus_act)




# butt_input_field_plus_to_one_2 = None
# butt_input_field_minus_to_one_2 = None



output_field_rand_num = Output_field([all_sprites], screen, [0.8, 0.1], 'c', [0.5, -0.425], (GREY, BLACK, (70, 70, 70, 255)), 'Output', 0.7, file_font_press_start)
output_field_rand_num.butt_generate = butt_generate


my_mouse = Mouse([all_sprites, sprites_event], screen, [20, 20], (0, 0, 0, 100))
my_mouse.add_indicated_events({
    'press_lkm': [my_mouse.change_color_press_lkm],
    'not_press': [my_mouse.change_color_not_press],
    
})
my_mouse.colors['color_press_lkm'] = (255, 255, 255, 127)