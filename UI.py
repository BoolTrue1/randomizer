# import os
# import pygame
from parameters import *
from random_generator import *

pygame.init()


font_press_start = pygame.font.Font(file_font_press_start, 60)


# Рассчитывает размер в px
def return_size_px(size_x, size_y, screen) -> list:
	# Если размер кнопки указано в дробном значении,
	# то оно воспринется в процентах
	if isinstance(size_x, float):
		size_x = screen[0] * size_x
	if isinstance(size_y, float):
		size_y = screen[1] * size_y
	# Если размер кнопки указан в отрицательном значении,
	# то 
	if size_x < 0:
		size_x = screen[0] + size_x
	if size_y < 0:
		size_y = screen[1] + size_y
	return [size_x, size_y]


# Рассчитывает позицию в px
def return_pos_px(pos_x, pos_y, screen) -> list:
	# Если размер кнопки указано в дробном значении,
	# то оно воспринется в процентах
	if isinstance(pos_x, float):
		pos_x = screen[0] * pos_x
	if isinstance(pos_y, float):
		pos_y = screen[1] * pos_y
	# Если положение кнопки указано в отрицательном значении,
	# то координаты её положения будут отщитываться с
	# противоположной стороны экрана
	if pos_x < 0:
		pos_x = screen[0] + pos_x
	if pos_y < 0:
		pos_y = screen[1] + pos_y
	return [pos_x, pos_y]


# Указывает определённую точку кнопки, относительно которой будет
# ориентиравано расположение самой кнопки
def return_pos_calc(anch_point, size_x, size_y, pos_x, pos_y) -> list:
	pos_calcul_x = pos_x
	pos_calcul_y = pos_y
	if anch_point == 'tl':
		pos_calcul_x = pos_x + size_x // 2
		pos_calcul_y = pos_y + size_y // 2
	elif anch_point == 't':
		pos_calcul_y = pos_y + size_y // 2
	elif anch_point == 'tr':
		pos_calcul_x = pos_x - size_x // 2
		pos_calcul_y = pos_y + size_y // 2
	elif anch_point == 'l':
		pos_calcul_x = pos_x + size_x // 2
	elif anch_point == 'c':
		pass
	elif anch_point == 'r':
		pos_calcul_x = pos_x - size_x // 2
	elif anch_point == 'bl':
		pos_calcul_x = pos_x + size_x // 2
		pos_calcul_y = pos_y - size_y // 2
	elif anch_point == 'b':
		pos_calcul_y = pos_y - size_y // 2
	elif anch_point == 'br':
		pos_calcul_x = pos_x - size_x // 2
		pos_calcul_y = pos_y - size_y // 2
	else:
		pos_calcul_x = pos_x + size_x // 2
		pos_calcul_y = pos_y + size_y // 2
	return [pos_calcul_x, pos_calcul_y]



class Button(pygame.sprite.Sprite):
	num_r = ''
	num_1 = ''
	num_2 = ''
	input_field_1 = None
	input_field_2 = None
	fl_imgs = False
	fl_text = False
	fl_exit = False
	field_text = None
	# self.font = pygame.font.Font(file_font_press_start, size_text)
	# self.txt_surface = self.font.render(self.text, True, self.colors['text'])

	def __init__(self, sp_grs: list, sc, size: list, anch_pnt: str, pos: list, color: tuple):
		pygame.sprite.Sprite.__init__(self)
		size = return_size_px(size[0], size[1], sc.get_size())
		pos = return_pos_px(pos[0], pos[1], sc.get_size())
		# Указание параметров объекта по его аргументам конструктора
		self.sp_grs = sp_grs
		self.screen = sc
		self.size = size
		self.anch_pnt = anch_pnt
		self.pos_input = pos
		self.pos_calc = pos
		self.colors = {
			'color_passive': color,
			'color_active': None,
			'color': color,
		}
		# self.image = pygame.Surface(self.size)
		# self.image.fill(color[0:3])
		# self.image.set_alpha(color[3])
		self.image = pygame.Surface(self.size, pygame.SRCALPHA)
		self.image.fill(color[0:4])
		self.rect = self.image.get_rect()
		self.indicated_events = {}
		# Указание точки кнопки, относительно которой будет
		# ориентиравано расположение самой кнопки
		self.pos_calc = return_pos_calc(anch_pnt, size[0], size[1], pos[0], pos[1])
		# Неиспользуемый словарь с точками привазки программе
		self.anch_pnts = {
			'tl': self.rect.topleft,
			't': self.rect.top,
			'tr': self.rect.topright,
			'l': self.rect.left,
			'c': self.rect.center,
			'r': self.rect.right,
			'bl': self.rect.bottomleft,
			'b': self.rect.bottom,
			'br': self.rect.bottomright
		}
		# self.anch_pnts[self.anch_pnt] = pos
		# self.anch_pnt = self.anch_pnts[anch_pnt]
		self.rect.center = self.pos_calc
		# self.pos_text_center = (self.rect.width // 2 - self.txt_surface.get_width() // 2,
		# 		self.rect.height // 2 - self.txt_surface.get_height() // 2)
		for sp_gr in sp_grs:
			sp_gr.add(self)

	# Метод, проверяющий любые события каждый цикл программы
	def event_check(self, event, keystate, mouse_key, mouse_pos):
		# Динамически изменяющийся словарь {'сокр. назв. соб.'  :  само событие}
		self.events_list = {
			'pointing': self.rect.collidepoint(mouse_pos[0], mouse_pos[1]),
			'not_pointing': not(self.rect.collidepoint(mouse_pos[0], mouse_pos[1])),
			'press': self.rect.collidepoint(mouse_pos[0], mouse_pos[1]) and event.type == pygame.MOUSEBUTTONDOWN and event.button == 1,
			'press_enter': event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN,
		}
		# Выполняет указанные действия кнопки, если соответствующее событие истинно
		for e in self.indicated_events:
			if self.events_list[e]:
				for action in self.indicated_events[e]:
					action()
	

	# Метод, обновляющий объект каждый цикл программы
	def update(self):
		#self.image = pygame.Surface(self.size)
		# self.image.fill(self.colors['color'][0:3])
		# self.image.set_alpha(self.colors['color'][3])
		self.image.fill(self.colors['color'][0:4])
		self.rect.center = self.pos_calc
		if self.input_field_1 != None and self.input_field_2 != None:
			self.num_1 = self.input_field_1.text
			self.num_2 = self.input_field_2.text
		if self.fl_text:
			self.txt_surface = self.font.render(self.text, True, self.text_colors['text'])
	
	def render(self):
		if self.fl_imgs:
			self.image.blit(self.imgs['img'], (0, 0))
		if self.fl_text:
			self.image.blit(self.txt_surface, (self.rect.width // 2 - self.txt_surface.get_width() // 2,
					self.rect.height // 2 - self.txt_surface.get_height() // 2))
		self.screen.blit(self.image, self.rect)
	
	# Выход из программы
	def game_quit(self):
		self.fl_exit = True
	
	def change_color_active(self):
		self.colors['color'] = self.colors['color_active']
	
	def change_color_passive(self):
		self.colors['color'] = self.colors['color_passive']
	
	def set_imgs(self, img_passive, img_active):
		self.imgs = {
			'img_passive': pygame.transform.scale(pygame.image.load(img_passive), self.size),
			'img_active': pygame.transform.scale(pygame.image.load(img_active), self.size),
			'img': pygame.transform.scale(pygame.image.load(img_passive), self.size),
		}
		self.fl_imgs = True
	
	def change_img_active(self):
		self.imgs['img'] = self.imgs['img_active']
	
	def change_img_passive(self):
		self.imgs['img'] = self.imgs['img_passive']
	
	def set_text(self, text: str, size_text, colors: list, font=None):
		self.fl_text = True
		self.text_colors = {
			'text_active': colors[1],
			'text_passive': colors[0],
			'text': colors[0],
		}
		if isinstance(size_text, float):
			size_text = int(self.size[1] * size_text)
		self.font = pygame.font.Font(font, size_text)
		self.text = text
		self.txt_surface = self.font.render(self.text, True, self.text_colors['text'])
	
	def change_color_text_active(self):
		self.text_colors['text'] = self.text_colors['text_active']
	
	def change_color_text_passive(self):
		self.text_colors['text'] = self.text_colors['text_passive']
	
	def generating(self):
		self.num_r = output_rand_num(self.num_1, self.num_2)
		# print(self.num_r) ##
	
	def change_value_field_plus_one(self):
		# text = self.field_text.text
		if self.field_text.text != '' and int(self.field_text.text) < 999:
			self.field_text.text = str(int(self.field_text.text) + 1)
		elif self.field_text.text == '':
			self.field_text.text = '0'
		self.field_text.forced_update()

	def change_value_field_minus_one(self):
		if self.field_text.text != '' and int(self.field_text.text) > 0:
			self.field_text.text = str(int(self.field_text.text) - 1)
		elif self.field_text.text == '':
			self.field_text.text = '0'
		self.field_text.forced_update()

	# Добавляет условия (в качестве ключа) и
	# соответственные действия кнопки (в качестве значения ключа)
	def add_indicated_events(self, events: dict):
		self.indicated_events = events



class Layers(pygame.sprite.Sprite):
	def __init__(self, sp_grs: list, sc, size: list, anch_pnt: str, pos: list, color: tuple, img: str):
		pygame.sprite.Sprite.__init__(self)
		size = return_size_px(size[0], size[1], sc.get_size())
		pos = return_pos_px(pos[0], pos[1], sc.get_size())
		# Указание параметров объекта по его аргументам конструктора
		self.sp_grs = sp_grs
		self.screen = sc
		self.size = size
		self.anch_pnt = anch_pnt
		self.pos_input = pos
		self.pos_calc = pos
		self.color = color
		self.image = pygame.Surface(self.size, pygame.SRCALPHA)
		self.image.fill(color[0:4])
		# self.image.set_alpha(color[3])
		self.rect = self.image.get_rect()
		if img != '':
			self.fl_img = True
		else:
			self.fl_img = False
		if self.fl_img:
			self.img = pygame.image.load(img)
			self.img_transf = pygame.transform.scale(self.img, self.size)
			self.image.blit(self.img_transf, (0, 0))
		# Указание точки кнопки, относительно которой будет
		# ориентиравано расположение самой кнопки
		self.pos_calc = return_pos_calc(anch_pnt, size[0], size[1], pos[0], pos[1])
		self.rect.center = self.pos_calc
		for sp_gr in sp_grs:
			sp_gr.add(self)
	
	def update(self):
		# self.image = pygame.Surface(self.size)
		# self.image.fill(self.color[0:4]) ## закоментировать
		# self.rect.center = self.pos_calc
		pass

	def render(self):
		if self.fl_img: ## закоментировать
			self.image.blit(self.img_transf, (0, 0)) ## закоментировать
		self.screen.blit(self.image, self.rect)
	
	def cleaning(self):
		# self.image.fill(self.color[0:4]) ## закоментировать
		pass



class Mouse(pygame.sprite.Sprite):
	def __init__(self, sp_grs: list, sc, size: list, color: tuple):
		pygame.sprite.Sprite.__init__(self)
		pygame.mouse.set_visible(False)
		self.mouse_pos = pygame.mouse.get_pos()
		self.pos = self.mouse_pos
		self.sp_grs = sp_grs
		self.screen = sc
		size = return_size_px(size[0], size[1], sc.get_size())
		# Указание параметров объекта по его аргументам конструктора
		self.size = size
		self.colors = {
			'color_not_press': color,
			'color_press_lkm': None,
			'color': color,
			'': None,
		}
		self.image = pygame.Surface(self.size)
		self.image.fill(color[0:3])
		self.image.set_alpha(color[3])
		self.rect = self.image.get_rect()
		self.indicated_events = {}
		self.rect.topleft = self.pos
		for sp_gr in sp_grs:
			sp_gr.add(self)
	
	# Метод, проверяющий любые события каждый цикл программы
	def event_check(self, event, keystate, mouse_key, mouse_pos):
		# Динамически изменяющийся словарь {'сокр. назв. соб.'  :  само событие}
		self.events_list = {
			'press_lkm': event.type == pygame.MOUSEBUTTONDOWN and event.button == 1,
			'pressing_lkm': mouse_key[0],
			'press_rkm': event.type == pygame.MOUSEBUTTONDOWN and event.button == 3,
			'pressing_rkm': mouse_key[2],
			'press_ckm': event.type == pygame.MOUSEBUTTONDOWN and event.button == 2,
			'pressing_ckm': mouse_key[1],
			'not_press': event.type == pygame.MOUSEBUTTONUP,
		}
		# Выполняет указанные действия кнопки, если соответствующее событие истинно
		for e in self.indicated_events:
			if self.events_list[e]:
				for action in self.indicated_events[e]:
					action()

	# Метод, обновляющий объект каждый цикл программы
	def update(self):
		#self.image = pygame.Surface(self.size)
		self.image.fill(self.colors['color'][0:3])
		self.image.set_alpha(self.colors['color'][3])
		self.mouse_pos = pygame.mouse.get_pos()
		self.pos = [self.mouse_pos[0], self.mouse_pos[1]]
		self.rect.topleft = self.pos
	
	def render(self):
		self.screen.blit(self.image, self.rect)
	
	# Добавляет условия (в качестве ключа) и
	# соответственные действия кнопки (в качестве значения ключа)
	def add_indicated_events(self, events: dict):
		self.indicated_events = events
	
	def change_color_press_lkm(self):
		self.colors['color'] = self.colors['color_press_lkm']
	
	def change_color_not_press(self):
		self.colors['color'] = self.colors['color_not_press']



class Input_field(pygame.sprite.Sprite):
	nums = ()

	def __init__(self, sp_grs: list, sc, size: list, anch_pnt: str, pos: list, colors: tuple, size_text, font=None):
		pygame.sprite.Sprite.__init__(self)
		size = return_size_px(size[0], size[1], sc.get_size())
		pos = return_pos_px(pos[0], pos[1], sc.get_size())
		# Указание параметров объекта по его аргументам конструктора
		self.sp_grs = sp_grs
		self.screen = sc
		self.size = size
		self.anch_pnt = anch_pnt
		self.pos_input = pos
		self.pos_calc = pos
		self.colors = {
			'text': colors[0],
			'surface_passive': colors[1],
			'surface_active': colors[2],
			'surface_pointing': colors[3],
			'surface_not_pointing': colors[1],
			'color': colors[1],
		}
		if isinstance(size_text, float):
			size_text = int(size[1] * size_text)
		self.font = pygame.font.Font(font, size_text)
		self.rect = pygame.Rect(pos[0], pos[1], size[0], size[1])
		self.text = ''
		self.txt_surface = self.font.render(self.text, True, self.colors['text'])
		self.image = pygame.Surface(size)
		self.active = False
		self.indicated_events = {}
		# Указание точки кнопки, относительно которой будет
		# ориентиравано расположение самой кнопки
		self.pos_calc = return_pos_calc(anch_pnt, size[0], size[1], pos[0], pos[1])
		self.rect.center = self.pos_calc
		for sp_gr in sp_grs:
			sp_gr.add(self)

	# Метод, проверяющий любые события каждый цикл программы
	def event_check(self, event, keystate, mouse_key, mouse_pos):
		# Динамически изменяющийся словарь {'сокр. назв. соб.'  :  само событие}
		self.events_list = {
			'pointing': self.rect.collidepoint(mouse_pos[0], mouse_pos[1]),
			'not_pointing': not(self.rect.collidepoint(mouse_pos[0], mouse_pos[1])),
			'press_here': self.rect.collidepoint(mouse_pos[0], mouse_pos[1]) and event.type == pygame.MOUSEBUTTONDOWN and event.button == 1,
			'press_not_here': not(self.rect.collidepoint(mouse_pos[0], mouse_pos[1])) and event.type == pygame.MOUSEBUTTONDOWN and event.button == 1,
			
		}
		# Выполняет указанные действия кнопки, если соответствующее событие истинно
		for e in self.indicated_events:
			if self.events_list[e]:
				for action in self.indicated_events[e]:
					action()
		if event.type == pygame.KEYDOWN:
			if self.active:
				if event.key == pygame.K_BACKSPACE:
					self.text = self.text[:-1]
				else:
					if len(self.text) < 3:
						for num in self.nums:
							if event.unicode == str(num):
								self.text += event.unicode
								break
				self.txt_surface = self.font.render(self.text, True, self.colors['text'])

	# Метод, обновляющий объект каждый цикл программы
	def update(self):
		if self.active:
			self.colors['color'] = self.colors['surface_active']
		# else:
		# 	self.colors['color'] = self.colors['surface_passive']
		self.image = pygame.Surface(self.size)
		self.image.fill(self.colors['color'][0:3])
		self.image.set_alpha(self.colors['color'][3])
		self.rect.center = self.pos_calc
	
	def forced_update(self):
		self.txt_surface = self.font.render(self.text, True, self.colors['text'])
	
	def render(self):
		self.image.blit(self.txt_surface, (self.rect.width // 2 - self.txt_surface.get_width() // 2,
				self.rect.height // 2 - self.txt_surface.get_height() // 2))
		self.screen.blit(self.image, self.rect)
	
	def change_color_active(self):
		self.colors['color'] = self.colors['surface_pointing']
	
	def change_color_passive(self):
		self.colors['color'] = self.colors['surface_not_pointing']
	
	def activating_input_field(self):
		self.active = True
		self.colors['color'] = self.colors['surface_active']
	
	def passivating_input_field(self):
		self.active = False
		self.colors['color'] = self.colors['surface_passive']

	# Добавляет условия (в качестве ключа) и
	# соответственные действия кнопки (в качестве значения ключа)
	def add_indicated_events(self, events: dict):
		self.indicated_events = events



class Output_field(pygame.sprite.Sprite):
	butt_generate = None

	def __init__(self, sp_grs: list, sc, size: list, anch_pnt: str, pos: list, colors: tuple, text: str, size_text, font=None):
		pygame.sprite.Sprite.__init__(self)
		size = return_size_px(size[0], size[1], sc.get_size())
		pos = return_pos_px(pos[0], pos[1], sc.get_size())
		# Указание параметров объекта по его аргументам конструктора
		self.sp_grs = sp_grs
		self.screen = sc
		self.size = size
		self.anch_pnt = anch_pnt
		self.pos_input = pos
		self.pos_calc = pos
		self.colors = {
			'text': colors[1],
			'text_default': colors[0],
			'surface_passive': colors[2],
			'color': colors[2],
		}
		if isinstance(size_text, float):
			size_text = int(size[1] * size_text)
		self.font = pygame.font.Font(font, size_text)
		self.rect = pygame.Rect(pos[0], pos[1], size[0], size[1])
		self.text = text
		self.text_default = text
		self.txt_surface = self.font.render(self.text, True, self.colors['text'])
		self.image = pygame.Surface(size)
		self.image.fill(self.colors['color'][0:3])
		self.image.set_alpha(self.colors['color'][3])
		self.active = False
		self.indicated_events = {}
		# Указание точки кнопки, относительно которой будет
		# ориентиравано расположение самой кнопки
		self.pos_calc = return_pos_calc(anch_pnt, size[0], size[1], pos[0], pos[1])
		self.rect.center = self.pos_calc
		for sp_gr in sp_grs:
			sp_gr.add(self)

	# Метод, обновляющий объект каждый цикл программы
	def update(self):
		# self.image = pygame.Surface(self.size)
		self.image.fill(self.colors['color'][0:3])
		self.image.set_alpha(self.colors['color'][3])
		self.rect.center = self.pos_calc
		if self.butt_generate != None:
			if self.butt_generate.num_r != '':
				self.text = self.butt_generate.num_r
				self.txt_surface = self.font.render(self.text, True, self.colors['text'])
			else:
				self.txt_surface = self.font.render(self.text_default, True, self.colors['text_default'])
		# self.txt_surface = self.font.render(self.text, True, self.colors['text'])
	
	def render(self):
		self.image.blit(self.txt_surface, (self.rect.width // 2 - self.txt_surface.get_width() // 2,
				self.rect.height // 2 - self.txt_surface.get_height() // 2))
		self.screen.blit(self.image, self.rect)