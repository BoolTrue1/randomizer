import random


def output_rand_num(r_num_1: str, r_num_2: str) -> str:
    if bool(r_num_1) == False or bool(r_num_2) == False:
        return 'Error'
    else:
        r_num_1 = int(r_num_1)
        r_num_2 = int(r_num_2)
        if r_num_1 > r_num_2:
            return 'Error'
        res = random.randint(r_num_1, r_num_2)
        return str(res)

